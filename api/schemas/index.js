const core = require('../core');

const userSchema = require('./users');
const channelsSchema = require('./channels');

module.exports.users = core.mongoose.model('users', userSchema);
module.exports.channels = core.mongoose.model('channels', channelsSchema);