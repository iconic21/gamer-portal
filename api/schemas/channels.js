const core = require('../core');

const channelSchema = core.mongoose.Schema({
    name: String,
    description: String,
    cover: String,
    logo: String,
    groupId: String,
    created_on: Date,
    updated_on: Date
});

channelSchema.pre("save", next => {
    const now = new Date();

    if (!this.created_on) {
        this.created_on = now;
    }
    
    this.updated_on = now;
    next();
});

module.exports = channelSchema;