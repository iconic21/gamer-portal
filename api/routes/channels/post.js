const core = require('./../../core');

module.exports = (apiRoutes) => {
    apiRoutes.post('/', core.services.authentication.auth, core.services.channels.createNewChannel);

    return apiRoutes;
}