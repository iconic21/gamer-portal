const core = require('./../../core');

module.exports = (apiRoutes) => {
    apiRoutes.get('/', core.services.authentication.auth, core.services.channels.getAllChannels);

    return apiRoutes;
}