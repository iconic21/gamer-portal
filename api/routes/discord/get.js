const core = require('./../../core');

module.exports = (apiRoutes) => {
    apiRoutes.get('/authUrl', core.services.discord.getAuthUrl);
    apiRoutes.get('/redirect', core.services.discord.returnAuth, core.services.discord.login);

    apiRoutes.get('/me/role', core.services.authentication.auth, core.services.discord.user.getUserRole);
    apiRoutes.get('/channels/groups', core.services.authentication.auth, core.services.discord.channels.getAllChannelGroups);
    apiRoutes.get('/channels/groups/:id/voiceChannels', core.services.authentication.auth, core.services.discord.channels.getVoiceChannels);
    apiRoutes.get('/channels/:id/inviteLink', core.services.authentication.auth, core.services.discord.channels.getInviteUrl);

    return apiRoutes;
}