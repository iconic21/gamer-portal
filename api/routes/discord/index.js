const core = require('./../../core');
const pathName = '/discord';

var apiRoutes = core.express.Router();
apiRoutes = require('./get')(apiRoutes);
core.app.use(pathName, apiRoutes);