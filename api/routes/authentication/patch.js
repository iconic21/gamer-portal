const core = require('../../core');

module.exports = (apiRoutes) => {
    apiRoutes.patch('/logout', core.services.authentication.logout);

    return apiRoutes;
}
