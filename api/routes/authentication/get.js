const core = require('../../core');

module.exports = (apiRoutes) => {
    apiRoutes.get('/validate', core.services.authentication.auth, (req, res) => {
        return res.ok(true, 200);
    });

    return apiRoutes;
}