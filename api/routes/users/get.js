const core = require('./../../core');

module.exports = (apiRoutes) => {
    apiRoutes.get('/me', core.services.authentication.auth, core.services.users.me);

    return apiRoutes;
}