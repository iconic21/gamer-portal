const core = require('./../../core');

const _getAllChannels = async () => {
    return await core.schema.channels.find();
}

const _createNewChannel = async channel => {
    const newChannel = await new core.schema.channels(channel).save();
    return newChannel;
}

const createNewChannel = async (req, res) => {
    const channel = {
        name: req.body.name,
        description: req.body.description,
        cover: req.body.cover,
        logo: req.body.logo,
        groupId: req.body.groupId,
    }
    const savedChannel = await _createNewChannel(channel);
    return res.ok(savedChannel, 201);
}

const getAllChannels = async (req, res) => {
    const channels = await _getAllChannels();
    return res.ok(channels);
}

module.exports = {
    createNewChannel,
    getAllChannels
}