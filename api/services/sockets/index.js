const core = require('./../../core');
const tokenService = require('./../authentication/token');

let connectedUsers = 0;

core.io.on('connection', socket => {
    socket.on('setToken', token => {
        tokenService.validateToken(token).then(tokens => {
            if (!tokens || !tokens['decoded']) {
                return;
            }

            socket.user = tokens['decoded'];
            connectedUsers++;
            console.log('connected users', connectedUsers);
        }, err => {
            console.log(err);
        });
    });

    socket.on('disconnect', () => {
        if (socket.user) {
            connectedUsers--;
        }
        console.log('connected users', connectedUsers);
    });
});