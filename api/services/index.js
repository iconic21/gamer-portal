module.exports = {
    common: require('./common'),
    users: require('./users'),
    authentication: require('./authentication'),
    discord: require('./discord'),
    channels: require('./channels')
}