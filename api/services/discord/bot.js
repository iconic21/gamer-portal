const Discord = require('discord.js');
const core = require('./../../core');
const client = new Discord.Client();

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
    if (msg.content === 'ping') {
        msg.reply('pong');
    }
});

client.login(core.config.discord.botToken);

module.exports = client;