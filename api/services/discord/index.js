const core = require('./../../core');
const Request = require('./request');
const auth = require('./../authentication');
const discordConfig = core.config.discord;

const user = require('./user');
const channels = require('./channels');

const _getAuthUrl = () => {
    const scope = 'identify%20email%20connections%20guilds%20guilds.join';
    return `${discordConfig.baseUrl}${discordConfig.authUrl}?client_id=${discordConfig.clientID}&redirect_uri=${discordConfig.redirectUrl}&response_type=code&scope=${scope}`;
}

const getAuthUrl = (req, res) => {
    return res.ok(_getAuthUrl(), 200);
}

const _getAccessToken = async code => {
    const payload =
    'client_id=' + discordConfig.clientID +
    '&client_secret=' + discordConfig.clientSecret +
    '&grant_type=authorization_code' +
    '&code=' + code +
    '&redirect_uri=' + discordConfig.redirectUrl +
    '&scope=identify email connections guilds guilds.join';

    return await Request.post('/oauth2/token', payload);
}

const returnAuth = async (req, res, next) => {
    const result = await _getAccessToken(req.query.code);
    req.discordToken = result.data;
    next();
}

const _refreshToken = async (refreshToken) => {
    const payload =
        'client_id=' + discordConfig.clientID +
        '&client_secret=' + discordConfig.clientSecret +
        '&grant_type=refresh_token' +
        '&refresh_token=' + refreshToken +
        '&redirect_uri=' + discordConfig.redirectUrl +
        '&scope=identify email connections guilds guilds.join';

    return await Request.post('/oauth2/token', payload);
}

const _getUserInfo = async token => {
    return await Request.get('/users/@me', token);
}

const _login = async (access_token, refresh_token) => {
    const result = await _getUserInfo(access_token);
    const currentUser = await core.services.users.findUserByEmail(result.data.email);
    if (currentUser) {
        return currentUser;
    }

    const user = _registerUser(result.data, access_token, refresh_token);
    return user;
}

const login = async (req, res) => {
    const user = await _login(req.discordToken.access_token, req.discordToken.refresh_token);
    await auth.setAccessCookies(res, user);
    return res.ok(user);
}

const _registerUser = async (user, access_token, refresh_token) => {
    const userInfo = {
        display_name: user.username,
        email: user.email,
        userIndex: user.discriminator,
        avatar: user.avatar,
        discordId: user.id,
        discordToken: access_token,
        discordRefreshToken: refresh_token
    }
    return await new core.schema.users(userInfo).save();
}

module.exports = {
    getAuthUrl,
    returnAuth,
    login,
    user,
    channels
}