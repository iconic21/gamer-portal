const core = require('./../../core');

const getUserRole = (req, res) => {
    const userId = req.session.user.discordId;
    const guild = core.discordBot.guilds.first();
    const user = guild.members.find(member => member.id === userId);
    if (user) {
        const role = user.roles.last();

        return res.ok({
            name: role.name,
            color: role.hexColor
        });
    }
    
    return res.error("User is not in guild", 404);
}

module.exports = {
    getUserRole
}