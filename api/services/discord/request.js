const core = require('./../../core');
const discordConfig = core.config.discord;

class Request {
    static get(path, token) {
        return new core.promise((resolve, reject) => {
            let config = {};
            if (token) {
                config = {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            }
            core.axios.get(`${discordConfig.baseUrl}${path}`, config).then(result => {
                resolve(result);
            }, err => {
                reject(err);
            })
        });
    }

    static post(path, payload, token) {
        return new core.promise((resolve, reject) => {
            const config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }

            if (token) {
                config = {
                    headers: {
                        ...config.headers,
                        'Authorization': `Bearer ${token}`
                    }
                }
            }

            core.axios.post(`${discordConfig.baseUrl}${path}`, payload, config).then(result => {
                resolve(result);
            }, err => {
                reject(err);
            })
        });
    }
}

module.exports = Request;