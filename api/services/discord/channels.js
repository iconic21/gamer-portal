const core = require('./../../core');

const getAllChannelGroups = async (req, res) => {
    const guild = core.discordBot.guilds.first();
    const category = {};

    guild.channels.filter(chan => chan.type === 'category').forEach(chan => {
        category[chan.id] = chan;
        category[chan.id].voiceChannels = [];
    });

    guild.channels.filter(chan => chan.type === 'voice').forEach(voc => {
        category[voc.parentID].voiceChannels = [...category[voc.parentID].voiceChannels, voc];
    });

    res.ok(category);
}

const getVoiceChannels = async (req, res) => {
    const guild = core.discordBot.guilds.first();
    const channels = guild.channels.filter(chan => chan.type === 'voice' && chan.parentID === req.params.id);
    const channelsList = channels.map(chan => {
        return {
            id: chan.id,
            name: chan.name,
            members: chan.members.map(mem => {
                return {
                    name: mem.displayName,
                    avatarUrl: mem.user.avatarURL
                }
            })
        };
    });
    res.ok(channelsList);
}

const getInviteUrl = async (req, res) => {
    const guild = core.discordBot.guilds.first();
    const channel = guild.channels.filter(chan => chan.id === req.params.id).first();
    if (!channel) {
        res.error('channel not found', 404);
    }

    const link = await channel.createInvite({
        maxAge: 180,
        maxUses: 5,
        unique: true
    }, '');

    res.ok(link.url);
}

module.exports = {
    getAllChannelGroups,
    getVoiceChannels,
    getInviteUrl
}