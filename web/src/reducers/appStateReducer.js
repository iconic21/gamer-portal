import * as types from './../actions/appState/types';

const defaultValue = {
    validatedUser: false
}

export default function(state = defaultValue, action) {
    switch(action.type) {
        case types.APPSTATE_SET_VALIDATDEUSER:
            return  {
                ...state,
                validatedUser: action.payload
            };
        default:
            return state;
    }
}