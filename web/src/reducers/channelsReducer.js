import * as types from '../actions/channels/types';

const defaultValue = {
    groups: {},
    channels: {}
}

export default function(state = defaultValue, action) {
    switch(action.type) {
        case types.CHANNELS_SET_DISCORD_GROUPS:
            return  {
                ...state,
                groups: action.payload
            };
        case types.CHANNELS_PATCH:
            return  {
                ...state,
                channels: {
                    ...state.channels,
                    [action.payload._id]: action.payload 
                }
            };
        case types.CHANNELS_FETCH:
            return  {
                ...state,
                channels: action.payload 
            };
        default:
            return state;
    }
}