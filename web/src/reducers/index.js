import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import usersReducer from './usersReducer';
import appStateReducer from './appStateReducer';
import channelsReducer from './channelsReducer';

export default history => combineReducers({
    router: connectRouter(history),
    users: usersReducer,
    appState: appStateReducer,
    channels: channelsReducer
});