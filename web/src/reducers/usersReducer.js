import * as types from './../actions/users/types';

const defaultValue = {
    me: null
}

export default function(state = defaultValue, action) {
    switch(action.type) {
        case types.USERS_SET_ME:
            return  {
                ...state,
                me: action.payload
            };
        case types.USERS_SET_ROLE:
            return  {
                ...state,
                me: {
                    ...state.me,
                    role: action.payload
                }
            };
        default:
            return state;
    }
}