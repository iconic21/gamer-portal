import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { usersActions } from './../../../actions';
import './styles.css';

class Login extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            authUrl: null,
            code: null
        }
    }

    componentWillMount() {
        let search = this.props.location.search;
        search = search.replace("?", '');
        const param = search.split('=');
        if (param[0] === 'code') {
            this.setState({
                code: param[1]
            });

            axios.get('/discord/redirect?code=' + param[1]).then(res => {
                this.props.users_set_me(res.data);
            });
        } else {
            axios.get('/discord/authUrl').then(res => {
                this.setState({
                    authUrl: res.data.data
                })
            });
        }
    }

    render() {
        return (
            <div className='login'>
                {
                    this.state.authUrl && 
                    <div onClick={this.handleLoginClick} className='login__discord'>
                        <i class="fab fa-discord"></i> Login with Discord
                    </div>
                }
            </div>
        );
    }

    handleLoginClick = () => {
        window.location.href = this.state.authUrl;
    }
}

const mapDispatchToProps = {
    ...usersActions
};

export default connect(null, mapDispatchToProps)(Login);