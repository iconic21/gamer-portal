import React, { PureComponent } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import Channels from './Channels';
import VoiceChannel from './Card/VoiceChannel';
import ActiveCard from './Card/ActiveCard';
import Card from './Card';
import './styles.css';

const posts = [
    {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }, {
        name: 'hello 1'
    }
]

class Home extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            columns: 3,
            channel: null,
            voiceChannels: []
        }
    }

    componentDidMount() {
        this.updateColumns();
        window.addEventListener('resize', this.updateColumns);
        this.updateSelectedChannel();
    }

    componentDidUpdate() {
        this.updateSelectedChannel();
    }

    render() {
        const postsColumns = [];
        for(let i = 0; i < this.state.columns; i++) {
            postsColumns.push([]);
        }

        for(let i = 0; i < this.state.voiceChannels.length; i++) {
            postsColumns[i%this.state.columns].push({
                type: 0,
                payload: this.state.voiceChannels[i]
            });
        }
        
        for(let i = 0; i < posts.length; i++) {
            postsColumns[i%this.state.columns].push({
                type: 1,
                payload: posts[i]
            });
        }

        return (
            <div className='home'>
                <div className='home__panel__side'>
                    <ActiveCard {...this.state.channel} />
                </div>
                <div className='home__panel__center'>
                    {
                        postsColumns.map((col, i) => 
                            <div className='home__panel__center__row' key={i}>
                                {
                                    col.map(post => {
                                        if (post.type === 1) {
                                            return <Card {...post.payload} />;
                                        } else {
                                            return <VoiceChannel {...post.payload} />
                                        }
                                    })
                                }
                            </div>
                        )
                    }
                </div>
                <div className='home__panel__side'>
                    <Channels />
                </div>
            </div>
        );
    }

    updateColumns = () => {
        let container = document.getElementsByClassName('home__panel__center')[0];
        this.setState({
            columns: Math.floor(container.clientWidth / 250)
        })
    }

    updateSelectedChannel = () => {
        const channelId = this.props.match.params.id;
        if (channelId) {
            const channel = this.props.channels.channels[channelId];
            if (channel && this.state.channel !== channel) {
                this.setState({
                    channel
                });

                axios.get(`/discord/channels/groups/${channel.groupId}/voiceChannels`).then(results => {
                    const data = results.data.data;
                    this.setState({
                        voiceChannels: data
                    });
                })
            }
        }
    }

    componentWillMount() {
        window.removeEventListener('resize', this.updateColumns);
    }
}

function mapStateToProps(state) {
    return {
        channels: state.channels,
    }
}

export default connect(mapStateToProps)(Home);