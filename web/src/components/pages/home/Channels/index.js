import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import Panel from './../../../common/panel';
import Channel from './Channel';
import Button from './../../../common/button';
import NewChannel from './NewChannel';
import { channelsActions } from './../../../../actions';
import './styles.css';

class Channels extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        }
    }

    componentDidMount() {
        axios.get('/discord/channels/groups')
        .then(results => this.props.channels_set_discord_groups(results.data.data));

        axios.get('/channels')
        .then(results => this.props.channels_fetch(results.data.data));
    }

    render() {

        const channelsList = [];
        Object.keys(this.props.channels.channels).forEach(key => {
            channelsList.push(this.props.channels.channels[key])
        })

        return (
            <Panel title={`channels (${channelsList.length})`}>
                {
                    channelsList.map(chan => {
                        return <Channel {...chan} key={chan.id} />
                    })
                }
                <Button onClick={this.openNewModal} >NEW CHANNEL</Button>
                <Button>BROWSE ALL CHANNELS</Button>
                <NewChannel onClose={this.closeNewModal} isOpen={this.state.isOpen} groups={this.props.channels.groups} />
            </Panel>
        );
    }

    openNewModal = () => this.setState({isOpen: true})

    closeNewModal = () => this.setState({isOpen: false})
}

function mapStateToProps(state) {
    return {
        channels: state.channels,
    }
}


const mapDispatchToProps = {
    ...channelsActions
};

export default connect(mapStateToProps, mapDispatchToProps)(Channels);