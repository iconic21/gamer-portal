import { Button, Classes, Dialog, Intent, InputGroup } from "@blueprintjs/core";
import axios from 'axios';
import { connect } from 'react-redux';
import React, { PureComponent } from 'react';
import { channelsActions } from '../../../../actions';

class Channel extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            logo: '',
            cover: '',
            groupId: ''
        }
    }
    render() {
        const groups = this.props.groups;
        const groupsList = [];

        Object.keys(groups).forEach(key => {
            groupsList.push(groups[key]);
        })

        return (
            <Dialog
                icon="info-sign"
                onClose={true}
                title="Palantir Foundry"
                {...this.props}
            >
                <div className={Classes.DIALOG_BODY}>
                    <InputGroup placeholder="Channel Name" onBlur={this.handleValueChange.bind(this, 'name')} />
                    <InputGroup placeholder="Channel Description" onBlur={this.handleValueChange.bind(this, 'description')} />
                    <InputGroup placeholder="Logo Url" onBlur={this.handleValueChange.bind(this, 'logo')} />
                    <InputGroup placeholder="Cover Url" onBlur={this.handleValueChange.bind(this, 'cover')} />
                    <select onChange={this.handleValueChange.bind(this, 'groupId')} >
                        <option value=''>--Channel Groups--</option>
                        {
                            groupsList.map(g => <option key={g.id} value={g.id}>{g.name}</option>)
                        }
                    </select>
                </div>
                <div className={Classes.DIALOG_FOOTER}>
                    <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                        <Button intent={Intent.PRIMARY} onClick={this.handleCreateBtnClick}>Create</Button>
                    </div>
                </div>
            </Dialog>
        );
    }

    handleValueChange = (prop, elm) => {
        this.setState({
            [prop]: elm.target.value
        })
    }

    handleCreateBtnClick = () => {
        axios.post('/channels', {
            ...this.state
        }).then(results => this.props.channels_patch(results.data.data));
    }
}

const mapDispatchToProps = {
    ...channelsActions
};

export default connect(null, mapDispatchToProps)(Channel);