import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

class Channel extends PureComponent {
    render() {
        return (
            <div className='channel'>
                <img src={this.props.cover} alt='cover' crossOrigin='Anonymous' />
                <div className='channel__info'>
                    <Link to={`/${this.props._id}`} className='channel__info__name'>{this.props.name}</Link>
                    {this.props.players && <div className='channel__info__players'>{this.props.players} Players</div>}
                </div>
            </div>
        );
    }
}

export default Channel;