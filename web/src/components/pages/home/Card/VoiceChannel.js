import React, { PureComponent } from 'react';
import axios from 'axios';

class VoiceChannel extends PureComponent {
    render() {
        return (
            <div className='card card__voice' onClick={this.handleOnClick} >
                <div className='card__voice__name'>{this.props.name}</div>
                <div className='card__voice__users'>
                    {
                        this.props.members.map(mem => {
                            return (
                                <div className='card__voice__users__user'>
                                    <div className='card__voice__users__user__avatar'>
                                        <img src={mem.avatarUrl ? mem.avatarUrl : '/assets/images/default-avatar.png'} alt='avatar' />
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        );
    }

    handleOnClick = () => {
        axios.get(`/discord/channels/${this.props.id}/inviteLink`).then(results => {
            window.open(results.data.data, '_blank');
        })
    }
}

export default VoiceChannel;