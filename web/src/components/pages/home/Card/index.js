import React, { PureComponent } from 'react';
import './styles.css';

class ActiveCard extends PureComponent {
    render() {
        return (
            <div className='card'>
                <div className='card__cover'>
                    <img src='https://media.giphy.com/media/D1lcEHYssYZ8Y/200w.webp' alt='cover' />
                </div>
                <div className='card__body'>
                    <div className='card__author'>
                        <div className='card__author__avatar'>
                            <img src='https://d2i72ju5buk5xz.cloudfront.net/gsc/ECP3Y7/66/01/80/66018044357b4496bcd3996811d2e511/images/home/u150.png?token=ceffea2ba270bc72296a2613b4a0843e' alt='cover' />
                        </div>
                        <div className='card__author__info'>
                            <div className='card__author__info__name'>DOTA 2</div>
                            <div className='card__author__info__date'>103,012 Players</div>
                        </div>
                    </div>
                    <div className='card__title'>
                        {this.props.name}
                    </div>
                    <div className='card__desc'>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                    </div>
                </div>
                <div className='card__footer'>
                    <div className='card__footer__left'>
                        <div className='card__footer__option'>
                            <i className="fas fa-heart"></i>
                            <div>12</div>
                        </div>
                        <div className='card__footer__option'>
                            <i className="fas fa-comments"></i>
                            <div>12</div>
                        </div>
                    </div>
                    <div className='card__footer__right'>
                        <div className='card__footer__option'>
                            <i className="fas fa-share-alt"></i>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ActiveCard;