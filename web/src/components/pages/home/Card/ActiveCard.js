import React, { PureComponent } from 'react';
import Button from './../../../common/button';
import './styles.css';

class ActiveCard extends PureComponent {
    render() {
        console.log(this.props);
        return (
            <div className='activecard'>
                <div className='activecard__cover'>
                    <img src={this.props.cover} alt='cover' />
                </div>
                <div className='activecard__body'>
                    <div className='activecard__info'>
                        <div className='activecard__info__logo'>
                            <img src={this.props.logo} alt='logo' />
                        </div>
                        <div className='activecard__info__details'>
                            <div className='activecard__info__details__name'>{this.props.name}</div>
                            {this.props.players && <div className='activecard__info__details__players'>103,012 Players</div>}
                        </div>
                    </div>
                    <div className='activecard__desc'>
                        {this.props.description}
                    </div>
                    <Button>JOIN THIS GROUP</Button>
                </div>
            </div>
        );
    }
}

export default ActiveCard;