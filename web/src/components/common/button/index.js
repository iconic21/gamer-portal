import React, { PureComponent } from 'react';
import './styles.css';

class Button extends PureComponent {
    render() {
        return (
            <div className='button' {...this.props} >
                <div>{this.props.children}</div>
            </div>
        );
    }
}

export default Button;