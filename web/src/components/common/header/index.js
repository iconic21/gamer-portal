import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Option from './Option';
import { request } from './../../../helpers';
import { usersActions } from './../../../actions';

import './styles.css';

const menuOptions = [
    {
        name: 'home',
        url: '/'
    }, {
        name: 'barracks',
        url: '/barracks'
    }, {
        name: 'armory',
        url: '/armory'
    }, {
        name: 'calendar',
        url: '/calendar'
    }, {
        name: 'forums',
        url: '/forums'
    }
]

class Header extends PureComponent {
    render() {
        const userRoleName = !this.props.users.me || !this.props.users.me.role ? '' : this.props.users.me.role.name;
        const userRoleColor = !this.props.users.me || !this.props.users.me.role ? '#AAAAAA' : this.props.users.me.role.color;

        return (
            <div className="header">
                <div className="header__options">
                    {
                        menuOptions.map(opt => <Option key={opt.url} active={opt.url === this.props.location.pathname} {...opt} />)
                    }
                </div>
                <div className="header__padding" />
                <div className="header__profile">
                    <div className="header__profile__info">
                        <div className="header__profile__info__name">
                            {this.props.users.me ? this.props.users.me.display_name : 'Loading...'}
                        </div>
                        <div className="header__profile__info__role" style={{color: userRoleColor}} >
                            {userRoleName}
                        </div>
                    </div>
                    <img src={!this.props.users.me || !this.props.users.me.avatar ? '/assets/images/default-avatar.png' : `https://cdn.discordapp.com/avatars/${this.props.users.me.discordId}/${this.props.users.me.avatar}.png`} alt='user avatar' />
                </div>
            </div>
        );
    }

    handleLogoutClick = () => {
        request.patch('/auth/logout').then(() => {
            this.props.users_set_me(null);
        });
    }
}

function mapStateToProps(state) {
    return {
        users: state.users
    }
}

const mapDispatchToProps = {
    ...usersActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
