import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { routerActions } from './../../../actions';
import './styles.css';

class Option extends PureComponent {
    render() {
        return (
            <Link
                className={`header__options__option ${this.props.active ? 'header__options__option--active' : ''}`}
                to={this.props.url}
                onClick={this.handleLinkClick}
            >
                {this.props.name}
            </Link>
        );
    }

    handleLinkClick = () => this.props.routerPushRoute(this.props.url);
}

const mapDispatchToProps = {
    ...routerActions
};

export default connect(null, mapDispatchToProps)(Option);
