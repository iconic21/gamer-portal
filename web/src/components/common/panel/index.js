import React, { PureComponent } from 'react';
import './styles.css';

class Panel extends PureComponent {
    render() {
        return (
            <div className='panel'>
                <div className='panel__title'>{this.props.title}</div>
                <div className='panel__body'>{this.props.children}</div>
            </div>
        );
    }
}

export default Panel;
