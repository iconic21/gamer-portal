import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Loadable from 'react-loadable';
import { withRouter } from 'react-router-dom';
import { routerActions, usersActions, appStateActions } from './actions';
import { request } from './helpers';
import { Switch, Route } from 'react-router-dom';

const Header = withRouter(Loadable({
    loader: () => import('./components/common/header'),
    loading: () => <span />,
}));

const Home = withRouter(Loadable({
    loader: () => import('./components/pages/home'),
    loading: () => <span />,
}));

const Login = withRouter(Loadable({
    loader: () => import('./components/pages/login'),
    loading: () => <span />,
}));

class App extends Component {
    componentWillMount() {
        this.validateLogin();
    }

    render() {
        const loggedIn = this.isLoggedIn();
        return (
            <div className="app">
                {!loggedIn && <Login />}
                {loggedIn && <Header />}
                <Switch>
                    {loggedIn && <Route exact={true} component={Home} path='/' />}
                    {loggedIn && <Route exact={true} component={Home} path='/:id' />}
                </Switch>
            </div>
        );
    }

    validateLogin = () => {
        if (this.props.appState.validatedUser) {
            return;
        }

        request.get('/auth/validate').then(() => {
            request.get('/users/me').then((response) => {
                this.props.users_set_me(response.data);
                request.get('/discord/me/role').then((response) => {
                    this.props.users_set_role(response.data);
                }, console.error);
            }, console.error);
        }, () => {
            this.props.users_set_me(null);
        }).then(() => this.props.appState_set_validatedUser(true));
    }

    isLoggedIn = () => {
        if (!this.props.appState.validatedUser || this.props.users.me) {
            return true;
        }

        return false;
    }
}

function mapStateToProps(state) {
    return {
        users: state.users,
        router: state.router,
        appState: state.appState
    }
}

const mapDispatchToProps = {
    ...routerActions,
    ...usersActions,
    ...appStateActions
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
