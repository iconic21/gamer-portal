import * as types from './types';

export const appState_set_validatedUser = (validated) => async dispatch => {
    dispatch({ type: types.APPSTATE_SET_VALIDATDEUSER, payload: validated });
};

export default {
    appState_set_validatedUser
};