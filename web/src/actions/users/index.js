import * as types from './types';

export const users_set_me = (me) => async dispatch => {
    dispatch({ type: types.USERS_SET_ME, payload: me });
};

export const users_set_role = (role) => async dispatch => {
    dispatch({ type: types.USERS_SET_ROLE, payload: role });
};

export default {
    users_set_me,
    users_set_role
};