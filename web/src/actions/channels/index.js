import * as types from './types';

export const channels_set_discord_groups = groups => async dispatch => {
    dispatch({ type: types.CHANNELS_SET_DISCORD_GROUPS, payload: groups });
};

export const channels_patch = chennel => async dispatch => {
    dispatch({ type: types.CHANNELS_PATCH, payload: chennel });
};

export const channels_fetch = chennels => async dispatch => {
    const channelsObj = {};
    chennels.forEach(chan => {
        channelsObj[chan._id] = chan;
    });
    dispatch({ type: types.CHANNELS_FETCH, payload: channelsObj });
};

export default {
    channels_set_discord_groups,
    channels_patch,
    channels_fetch
}