import axios from 'axios';

const setSymbolFor = (symbolKey, source, payload) => {
    if (!payload) {
        return source;
    }

    const symbol = Symbol.for(symbolKey);
    source[symbol] = payload;
    return source;
}

const getSymbolFor = (symbolKey, payload) => {
    if (!payload) {
        return undefined;
    }

    const symbol = Symbol.for(symbolKey);
    return payload[symbol];
}

export default {
    setSymbolFor,
    getSymbolFor
}

export const request = {
    post: (path, payload) => apiRequest(axios.post, path, payload),
    get: (path) => apiRequest(axios.get, path),
    put: (path, payload) => apiRequest(axios.put, path, payload),
    patch: async (path, payload) => await apiRequest(axios.patch, path, payload),
    delete: (path) => apiRequest(axios.delete, path)
}

const apiRequest = (request, path, payload) => {
    return new Promise((resolve, reject) => {
        let req = null;

        if (payload) {
            req = request(path, payload);
        } else {
            req = request(path);
        }
    
        req.then(res => resolve(res.data), err => reject(err.response))
        .catch(err => reject(err));
    });
}